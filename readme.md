# Feedback collector

A simple tool for a feedback gathering that demonstrates the REST API capabilities.

## How to run the application

### Prerequisites
* Install the latest *Java SE Development Kit 8*.
* Install any *git* distribution on your development machine.
* Checkout this source code branch.
* Verify that TCP port 8080 is free. In case when it is not, change the property *server.port* in */src/main/resources/application.properties* to any 
    other free TCP port. 

### Application launch 
* Navigate into the root of checked out source code on your filesystem.
* Run the following command from the checked out source code root: `gradlew bootRun`

### Application tests
* Navigate into the root of checked out source code on your filesystem.
* Run the following command from the checked out source code root: `gradlew test`
* You can see the test report at */build/reports/tests/test/index.htm*

## API documentation

The REST API is hosted on *http://* protocol on the port defined in the main application's property file (see *Prerequisites* chapter of this document 
for more details). 

You can reach it at *http://localhost:8080* by default.

The following endpoints are available:

### Create new feedback record

The endpoint is used to create a new feedback record.

#### URL

`/v1/feedback`

#### Method

`POST`

#### URL parameters

The REST API endpoint does not support any URL parameter.

#### Request data

The endpoint accepts data in *application/json* content type encoded in UTF-8.
All the fields are required and must not be set to an empty string or null value.

    {
        "user": "mort",
        "message": "feedback content"
    }

#### Success response

**Code:** 201

**Content:** The response does not contain any content.

**Headers:** The `Location` header is set to a resource that points to a newly saved record.

#### Error response

The error is thrown in case when either required *user* or *message* fields are missing or set to an empty string or null value.

**Code:** 400

**Content:**

    {
        "timestamp": "2017-08-15T20:20:34.293+0000",
        "status": 400,
        "error": "Bad Request",
        "exception": "com.xmort.web.exception.FeedbackNotValidException",
        "message": "The posted request is not correct. Either the 'user' or 'message' value was not set!",
        "path": "/v1/feedback"
    }

#### Sample call

This is an example of the call to the API endpoint:
       
    curl -H "Content-Type: application/json" -X POST -d '{"user":"mort","message":"feedback content"}' http://localhost:8080/v1/feedback
 
### Get a single feedback record

The endpoint is used to obtain a single feedback record identified by its ID.

#### URL

`/v1/feedback/{id}`

#### Method

`GET`

#### URL parameters

The REST API endpoint does not support any URL parameter.

#### Request data

The REST API endpoint does not accept any request payload.

#### Success response

**Code:** 200

**Content:**

    {
        "id": 1,
        "user": "mort",
        "message": "feedback content",
        "created": "2017-08-15T20:02:26.140Z"
    }
    
#### Error response

The error is thrown when the `id` path variable is set to a value that does not match any record.

**Code:** 404

**Content:** 

    {
        "timestamp": "2017-08-15T20:24:37.923+0000",
        "status": 404,
        "error": "Not Found",
        "exception": "com.xmort.web.exception.FeedbackNotFoundException",
        "message": "The feedback record with ID '8' has not been found!",
        "path": "/v1/feedback/8"
    }

The error is thrown when the `id` path variable is not a number.

**Code:** 400

**Content:** 

    {
        "timestamp": "2017-08-15T20:27:51.163+0000",
        "status": 400,
        "error": "Bad Request",
        "exception": "org.springframework.web.method.annotation.MethodArgumentTypeMismatchException",
        "message": "Failed to convert value of type 'java.lang.String' to required type 'java.lang.Long'; nested exception is java.lang.NumberFormatException: For input string: \"abc\"",
        "path": "/v1/feedback/abc"
    }

#### Sample call

This is an example of the call to the API endpoint:
       
    curl -X GET http://localhost:8080/v1/feedback/1
 
### Get all feedback records

The endpoint is used to obtain the list of all stored feedback records.

#### URL

`/v1/feedback`

#### Method

`GET`

#### URL parameters

The REST API endpoint does not support any URL parameter.

#### Request data

The REST API endpoint does not accept any request payload.

#### Success response

**Code:** 200

**Content:**

    [
        {
            "id": 1,
            "user": "mort",
            "message": "feedback content",
            "created": "2017-08-15T20:02:26.140Z"
        },
        {
            "id": 2,
            "user": "jdoe",
            "message": "another feedback",
            "created": "2017-08-15T20:11:08.500Z"
        }
    ]
    
#### Error response

There is no documented error response. 

#### Sample call

This is an example of the call to the API endpoint:
       
    curl -X GET http://localhost:8080/v1/feedback
 
### Get feedback records by user

The endpoint is used to obtain the list of all stored feedback records that were created by a certain user.

#### URL

`/v1/feedback/user`

#### Method

`GET`

#### URL parameters

`name=[exact string name of the user]`

The parameter is not required. In case when parameter is not provided or its value is empty, the list of all stored feedback records is returned.

#### Request data

The REST API endpoint does not accept any request payload.

#### Success response

**Code:** 200

**Content:**

    [
        {
            "id": 1,
            "user": "mort",
            "message": "feedback content",
            "created": "2017-08-15T20:02:26.140Z"
        },
        {
            "id": 2,
            "user": "mort",
            "message": "yet another feedback",
            "created": "2017-08-15T20:11:08.500Z"
        }
    ]
    
#### Error response

There is no documented error response.

#### Sample call

This is an example of the call to the API endpoint:
       
    curl -X GET http://localhost:8080/v1/feedback/user?name=mort
