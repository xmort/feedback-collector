package com.xmort.domain.entity;

import java.time.Instant;

import org.testng.Assert;
import org.testng.annotations.Test;

import nl.jqno.equalsverifier.EqualsVerifier;

@Test(groups = "unit")
public class FeedbackTest {

    @Test
    public void givenFeedback_whenUserAndMessageAreProvided_thenUserAndMessagesAreSet() {
        final String user = "mort";
        final String message = "message";
        final Feedback feedback = new Feedback(user, message);
        Assert.assertEquals(feedback.getUser(), user);
        Assert.assertEquals(feedback.getMessage(), message);
    }

    @Test
    public void givenSetupDateOfCreation_whenDateOfCreationIsNotSet_thenDateOfCreationIsSet() {

        final Feedback feedback = new Feedback();
        Assert.assertNull(feedback.getCreated());

        feedback.setupDateOfCreation();
        Assert.assertNotNull(feedback.getCreated());
    }

    @Test
    public void givenSetupDateOfCreation_whenDateOfCreationIsSet_thenDateOfCreationIsNotUpdated() {

        final Feedback feedback = new Feedback();
        feedback.setCreated(Instant.MIN);
        final Instant originalDateOfCreation = feedback.getCreated();

        feedback.setupDateOfCreation();

        Assert.assertEquals(feedback.getCreated(), originalDateOfCreation);
    }

    @Test
    public void testFeedbackEqualsContract() {
        EqualsVerifier
                .forClass(Feedback.class)
                .usingGetClass()
                .verify();
    }
}