package com.xmort.service;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.easymock.EasyMock;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.xmort.domain.dao.FeedbackDao;
import com.xmort.domain.entity.Feedback;

@Test(groups = "unit")
public class DefaultFeedbackServiceTest {

    private static final String USER = "mort";
    private static final String MESSAGE = "message";

    private FeedbackDao feedbackDao;
    private DefaultFeedbackService testedService;

    @BeforeMethod
    private void setupMocks() {
        feedbackDao = EasyMock.createMock(FeedbackDao.class);
        testedService = new DefaultFeedbackService(feedbackDao);
    }

    private void replayMocks() {
        EasyMock.replay(feedbackDao);
    }

    @AfterMethod
    private void verifyMocks() {
        EasyMock.verify(feedbackDao);
    }

    @Test
    public void givenSave_whenFeedbackIsProvided_thenItIsSaved() {

        final Feedback savedFeedback = new Feedback(USER, MESSAGE);
        final Feedback expectedFeedback = new Feedback(USER, MESSAGE);
        expectedFeedback.setId(1L);
        expectedFeedback.setCreated(Instant.now());

        EasyMock.expect(feedbackDao.save(savedFeedback)).andReturn(expectedFeedback);
        replayMocks();

        final Feedback updatedFeedback = testedService.save(savedFeedback);
        Assert.assertEquals(updatedFeedback, expectedFeedback);
    }

    @Test
    public void givenFindOne_whenFeedbackIsNotKnown_thenNothingIsReturned() {

        final long id = 1L;

        EasyMock.expect(feedbackDao.findOne(id)).andReturn(null);
        replayMocks();

        final Optional<Feedback> foundFeedback = testedService.findOne(id);
        Assert.assertFalse(foundFeedback.isPresent());
    }

    @Test
    public void givenFindOne_whenFeedbackIsKnown_thenItIsReturned() {

        final long id = 1L;

        final Feedback expectedFeedback = new Feedback(USER, MESSAGE);
        expectedFeedback.setId(id);
        expectedFeedback.setCreated(Instant.now());

        EasyMock.expect(feedbackDao.findOne(id)).andReturn(expectedFeedback);
        replayMocks();

        final Optional<Feedback> foundFeedback = testedService.findOne(id);
        Assert.assertTrue(foundFeedback.isPresent());
        Assert.assertEquals(foundFeedback.orElse(null), expectedFeedback);
    }

    @Test
    public void givenFindAll_whenSomeFeedbackRecordsAreKnown_thenTheirCollectionIsReturned() {

        final Feedback expectedFeedback = new Feedback(USER, MESSAGE);
        expectedFeedback.setId(1L);
        expectedFeedback.setCreated(Instant.now());

        final List<Feedback> expectedFeedbackRecords = Collections.singletonList(expectedFeedback);

        EasyMock.expect(feedbackDao.findAll()).andReturn(expectedFeedbackRecords);
        replayMocks();

        final List<Feedback> foundFeedbackRecords = testedService.findAll();
        Assert.assertEquals(foundFeedbackRecords, expectedFeedbackRecords);
    }

    @Test
    public void givenFindAllByUser_whenSomeFeedbackRecordFromUsersAreKnown_thenTheirCollectionIsReturned() {

        final Feedback expectedFeedback = new Feedback(USER, MESSAGE);
        expectedFeedback.setId(1L);
        expectedFeedback.setCreated(Instant.now());

        final List<Feedback> expectedFeedbackRecords = Collections.singletonList(expectedFeedback);

        EasyMock.expect(feedbackDao.findAllByUser(USER)).andReturn(expectedFeedbackRecords);
        replayMocks();

        final List<Feedback> foundFeedbackRecords = testedService.findAllByUser(USER);
        Assert.assertEquals(foundFeedbackRecords, expectedFeedbackRecords);
    }
}