package com.xmort.web;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.easymock.EasyMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.xmort.domain.entity.Feedback;
import com.xmort.service.FeedbackService;
import com.xmort.web.exception.FeedbackNotFoundException;
import com.xmort.web.exception.FeedbackNotValidException;
import com.xmort.web.form.NewFeedbackRequestForm;

@Test(groups = "unit")
public class FeedbackControllerTest {

    private static final long ID = 1L;
    private static final String USER = "mort";
    private static final String MESSAGE = "message";
    private static final Instant CREATED = Instant.now();

    private FeedbackService feedbackService;
    private FeedbackController testedController;

    @BeforeMethod
    private void setupMocks() {
        final MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        feedbackService = EasyMock.createMock(FeedbackService.class);
        testedController = new FeedbackController(feedbackService);
    }

    private void replayMocks() {
        EasyMock.replay(feedbackService);
    }

    @AfterMethod
    private void verifyMocks() {
        EasyMock.verify(feedbackService);
    }

    @Test(expectedExceptions = FeedbackNotFoundException.class)
    public void givenFindOneFeedbackRecords_whenRecordIsNotKnown_thenTheExceptionIsThrown() {

        final Long id = 1L;
        EasyMock.expect(feedbackService.findOne(id)).andReturn(Optional.empty());
        replayMocks();

        testedController.findOneFeedbackRecords(id);
    }

    @Test
    public void givenFindOneFeedbackRecords_whenRecordIsKnown_thenItIsReturned() {

        final Feedback expectedFeedback = buildFeedback(ID, USER, MESSAGE, CREATED);
        EasyMock.expect(feedbackService.findOne(ID)).andReturn(Optional.of(expectedFeedback));
        replayMocks();

        final Feedback foundFeedback = testedController.findOneFeedbackRecords(ID);
        Assert.assertEquals(foundFeedback, expectedFeedback);
    }

    @Test
    public void givenFindAllFeedbackRecords_whenSomeRecordAreFound_thenTheyAreReturned() {

        final Feedback expectedFeedback = buildFeedback(ID, USER, MESSAGE, CREATED);
        final List<Feedback> expectedFeedbackRecords = Collections.singletonList(expectedFeedback);
        EasyMock.expect(feedbackService.findAll()).andReturn(expectedFeedbackRecords);
        replayMocks();

        final List<Feedback> foundFeedbackRecords = testedController.findAllFeedbackRecords();
        Assert.assertEquals(foundFeedbackRecords, expectedFeedbackRecords);
    }

    @Test
    public void givenFindAllFeedbackRecordsByUser_whenSomeRecordByUserAreFound_thenTheyAreReturned() {

        final Feedback expectedFeedback = buildFeedback(ID, USER, MESSAGE, CREATED);
        final List<Feedback> expectedFeedbackRecords = Collections.singletonList(expectedFeedback);
        EasyMock.expect(feedbackService.findAllByUser(USER)).andReturn(expectedFeedbackRecords);
        replayMocks();

        final List<Feedback> foundFeedbackRecords = testedController.findAllFeedbackRecordsByUser(USER);
        Assert.assertEquals(foundFeedbackRecords, expectedFeedbackRecords);
    }

    @Test(expectedExceptions = FeedbackNotValidException.class)
    public void givenCreateFeedbackRecord_whenFeedbackFormIsNotValid_thenExceptionIsThrown() {
        final NewFeedbackRequestForm invalidForm = new NewFeedbackRequestForm();
        replayMocks();
        testedController.createFeedbackRecord(invalidForm);
    }

    @Test
    public void givenCreateFeedbackRecord_whenFeedbackFormIsValid_thenItIsSavedAndExpectedResponseIsReturned() {

        final NewFeedbackRequestForm validForm = new NewFeedbackRequestForm();
        validForm.setUser(USER);
        validForm.setMessage(MESSAGE);

        final Feedback expectedSavedFeedback = new Feedback(USER, MESSAGE);
        final Feedback storedFeedback = new Feedback(USER, MESSAGE);
        storedFeedback.setId(ID);
        storedFeedback.setCreated(CREATED);

        EasyMock.expect(feedbackService.save(expectedSavedFeedback)).andReturn(expectedSavedFeedback);
        replayMocks();

        final ResponseEntity<?> response = testedController.createFeedbackRecord(validForm);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        Assert.assertTrue(response.getHeaders().containsKey("Location"));
    }

    private static Feedback buildFeedback(final Long id, final String user, final String message, final Instant created) {
        final Feedback feedback = new Feedback(user, message);
        feedback.setId(id);
        feedback.setCreated(created);
        return feedback;
    }
}