package com.xmort.web.form;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.xmort.domain.entity.Feedback;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

@Test(groups = "unit")
public class NewFeedbackRequestFormTest {

    @DataProvider(name = "invalidForms")
    private Object[][] invalidForms() {
        return new Object[][]{
                {buildNewFeedbackRequestForm(null, "message")},
                {buildNewFeedbackRequestForm("", "message")},
                {buildNewFeedbackRequestForm("mort", null)},
                {buildNewFeedbackRequestForm("mort", "")}
        };
    }

    @Test(dataProvider = "invalidForms")
    public void givenIsValid_whenObjectIsInInvalidState_thenFalseIsReturned(final NewFeedbackRequestForm invalidForm) {
        Assert.assertFalse(invalidForm.isValid());
    }

    @Test
    public void givenIsValid_whenObjectIsInValidState_thenTrueIsReturned() {
        final NewFeedbackRequestForm validForm = buildNewFeedbackRequestForm("mort", "message");
        Assert.assertTrue(validForm.isValid());
    }

    @Test
    public void givenAsFeedback_whenObjectIsPopulated_thenFeedbackFormIsReturned() {
        final String user = "mort";
        final String message = "message";
        final NewFeedbackRequestForm form = buildNewFeedbackRequestForm(user, message);
        final Feedback feedback = form.asFeedback();
        final Feedback expectedFeedback = new Feedback(user, message);
        Assert.assertEquals(feedback, expectedFeedback);
    }

    @Test
    public void testNewFeedbackRequestFormEqualsContract() {
        EqualsVerifier
                .forClass(NewFeedbackRequestForm.class)
                .usingGetClass()
                .suppress(Warning.NONFINAL_FIELDS)
                .verify();
    }

    private static NewFeedbackRequestForm buildNewFeedbackRequestForm(final String user, final String message) {
        final NewFeedbackRequestForm requestForm = new NewFeedbackRequestForm();
        requestForm.setUser(user);
        requestForm.setMessage(message);
        return requestForm;
    }

}