package com.xmort.web.exception;

import org.testng.annotations.Test;

@Test(groups = "unit")
public class FeedbackNotValidExceptionTest {

    @Test(expectedExceptions = FeedbackNotValidException.class, expectedExceptionsMessageRegExp = "error")
    public void givenFeedbackNotValidException_whenMessageIsProvided_thenErrorMessageIsTheSame() {
        throw new FeedbackNotValidException("error");
    }

}