package com.xmort.web.exception;

import org.testng.annotations.Test;

@Test(groups = "unit")
public class FeedbackNotFoundExceptionTest {

    @Test(expectedExceptions = FeedbackNotFoundException.class, expectedExceptionsMessageRegExp = ".*'42'.*")
    public void givenFeedbackNotFoundException_whenIdIsProvided_thenItIsInFoundInErrorMessage() {
        throw new FeedbackNotFoundException(42L);
    }
}