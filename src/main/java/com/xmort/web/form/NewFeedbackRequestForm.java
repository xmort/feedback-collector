package com.xmort.web.form;

import java.io.Serializable;
import java.util.Objects;
import java.util.StringJoiner;

import org.h2.util.StringUtils;
import org.jetbrains.annotations.NotNull;

import com.xmort.domain.entity.Feedback;

/**
 * Data received from the HTTP request that contains information necessary to create new {@link Feedback} record.
 */
public class NewFeedbackRequestForm implements Serializable {

    private static final long serialVersionUID = 8785574038778433961L;

    private String user;
    private String message;

    /**
     * Convert data held by this class to the {@link Feedback} entity.
     *
     * @return {@link Feedback} entity with data set from the state of the parent object.
     */
    @NotNull
    public Feedback asFeedback() {
        return new Feedback(user, message);
    }

    /**
     * Determine if object is in valid state or not.
     *
     * @return True if both {@code user} and {@code message} are set to non empty value, false if otherwise.
     */
    public boolean isValid() {
        return !StringUtils.isNullOrEmpty(user) && !StringUtils.isNullOrEmpty(message);
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NewFeedbackRequestForm that = (NewFeedbackRequestForm) o;
        return Objects.equals(user, that.user) &&
                Objects.equals(message, that.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, message);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "CreateFeedbackRequestForm{", "}")
                .add("user='" + user + "'")
                .add("message='" + message + "'")
                .toString();
    }

    public String getUser() {
        return user;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }
}
