package com.xmort.web;

import java.net.URI;
import java.util.List;
import java.util.Objects;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.xmort.domain.entity.Feedback;
import com.xmort.service.FeedbackService;
import com.xmort.web.exception.FeedbackNotFoundException;
import com.xmort.web.exception.FeedbackNotValidException;
import com.xmort.web.form.NewFeedbackRequestForm;

/**
 * The REST controller that exports functionality related to {@link Feedback} records.
 */
@RestController
@RequestMapping(value = "/v1/feedback")
public class FeedbackController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackController.class);

    private static final String FIND_BY_ID_REQUEST_HANDLER_URI = "/{id}";
    private static final String FIND_BY_USER_REQUEST_HANDLER_URI = "/user";

    private final FeedbackService feedbackService;

    @Autowired
    public FeedbackController(final FeedbackService feedbackService) {
        this.feedbackService = Objects.requireNonNull(feedbackService, "'feedbackService' parameter must not be null!");
    }

    /**
     * Find and return single stored feedback record.
     *
     * @return The feedback record matching the provided {@code id}. In case when no record is found HTTP 404 code is returned instead.
     */
    @NotNull
    @RequestMapping(value = FIND_BY_ID_REQUEST_HANDLER_URI, method = RequestMethod.GET)
    public Feedback findOneFeedbackRecords(@PathVariable Long id) {
        LOGGER.info("Request to get stored feedback record with ID '{}'", id);
        return feedbackService.findOne(id)
                .orElseThrow(() -> new FeedbackNotFoundException(id));
    }

    /**
     * Find and return all the stored feedback records.
     *
     * @return The collections of found feedback records represented by the {@link Feedback} entity. Empty collection is returned when no feedback
     * record has been found.
     */
    @NotNull
    @RequestMapping(method = RequestMethod.GET)
    public List<Feedback> findAllFeedbackRecords() {
        LOGGER.info("Request to list all stored feedback records");
        return feedbackService.findAll();
    }

    /**
     * Find and return all the stored feedback records that were created by the user with provided {@code name}.
     *
     * @param name
     *         The name of the user we are searching for. The parameter can be null or omitted from the request. In that case all the stored feedback
     *         records are returned.
     * @return The collections of found feedback records represented by the {@link Feedback} entity. Empty collection is returned when no matching
     * feedback record has been found.
     */
    @NotNull
    @RequestMapping(value = FIND_BY_USER_REQUEST_HANDLER_URI, method = RequestMethod.GET)
    public List<Feedback> findAllFeedbackRecordsByUser(@Nullable @RequestParam(required = false) final String name) {
        LOGGER.info("Request to list stored feedback records filter by the user: '{}'", name);
        return feedbackService.findAllByUser(name);
    }

    /**
     * Store a new feedback record from the provided {@link NewFeedbackRequestForm} data.
     *
     * @param feedbackForm
     *         The request data with the feedback that will be stored.
     * @return HTTP 201 Created status code with Location header pointing to a newly created feedback record.
     */
    @NotNull
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> createFeedbackRecord(@NotNull @RequestBody final NewFeedbackRequestForm feedbackForm) {
        Objects.requireNonNull(feedbackForm, "'feedbackForm' parameter must not be null!");
        LOGGER.info("Request to store a new feedback record: '{}'", feedbackForm);
        if (feedbackForm.isValid()) {
            return persistFeedbackRecord(feedbackForm);
        } else {
            throw new FeedbackNotValidException("The posted request is not correct. Either the 'user' or 'message' value was not set!");
        }
    }

    private ResponseEntity<?> persistFeedbackRecord(final NewFeedbackRequestForm feedbackForm) {
        final Feedback savedRecord = feedbackService.save(feedbackForm.asFeedback());
        final URI savedRecordIUriLocation = findRecordUriLocation(savedRecord, FIND_BY_ID_REQUEST_HANDLER_URI);
        return ResponseEntity
                .created(savedRecordIUriLocation)
                .build();
    }

    private URI findRecordUriLocation(final Feedback savedRecord, final String requestHandlerUri) {
        return ServletUriComponentsBuilder
                .fromCurrentRequest().path(requestHandlerUri)
                .buildAndExpand(savedRecord.getId()).toUri();
    }
}
