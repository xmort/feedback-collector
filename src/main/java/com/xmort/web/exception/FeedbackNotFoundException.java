package com.xmort.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.xmort.domain.entity.Feedback;

/**
 * The exception used when requested {@link Feedback} record has not been found.
 * The exception sets HTTP 404 Not found status code.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class FeedbackNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 7598491627420998667L;

    /**
     * Create new instance of {@link FeedbackNotFoundException}.
     *
     * @param id
     *         The ID of the {@link Feedback} record that has not been found.
     */
    public FeedbackNotFoundException(final Long id) {
        super(String.format("The feedback record with ID '%s' has not been found!", id));
    }
}
