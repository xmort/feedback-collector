package com.xmort.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.xmort.domain.entity.Feedback;

/**
 * The exception used when posted data for creation of the {@link Feedback} record are not valid.
 * The exception sets HTTP 400 Bad request status code.
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FeedbackNotValidException extends RuntimeException {

    private static final long serialVersionUID = -8562621733521280276L;

    /**
     * Create new instance of {@link FeedbackNotValidException}.
     */
    public FeedbackNotValidException(final String message) {
        super(message);
    }
}
