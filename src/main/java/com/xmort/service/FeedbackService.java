package com.xmort.service;

import java.util.List;
import java.util.Optional;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.xmort.domain.entity.Feedback;

/**
 * The business service that is used to manage {@link Feedback} entities.
 */
public interface FeedbackService {

    /**
     * Create or update provided {@link Feedback} in the persistence layer.
     *
     * @param feedback
     *         The feedback that must be stored into the persistence layer.
     * @return The updated feedback.
     */
    @NotNull
    Feedback save(@NotNull Feedback feedback);

    /**
     * Find single stored {@link Feedback} record by its {@code id}.
     *
     * @param id
     *         The ID under which the record is stored.
     * @return Optional {@link Feedback} record matching the provided {@code id}.
     */
    @NotNull
    Optional<Feedback> findOne(@NotNull Long id);

    /**
     * Find all stored {@link Feedback} records.
     *
     * @return The collections of stored {@link Feedback} records. In case when no record is found, the empty collection is returned.
     */
    @NotNull
    List<Feedback> findAll();

    /**
     * Find all {@link Feedback} records that were sent by the provided {@code user}.
     *
     * @param user
     *         The exact user name that the records will be searched for.
     * @return The collection of matching records. In case when no records are matching to the provided {@code user}, the empty collection is returned.
     */
    @NotNull
    List<Feedback> findAllByUser(@Nullable String user);
}
