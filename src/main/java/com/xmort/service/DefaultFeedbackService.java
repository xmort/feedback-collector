package com.xmort.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.transaction.Transactional;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xmort.domain.dao.FeedbackDao;
import com.xmort.domain.entity.Feedback;

/**
 * The default implementation of the business service that is used to manage {@link Feedback} entities.
 */
@Service
public class DefaultFeedbackService implements FeedbackService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFeedbackService.class);

    private final FeedbackDao feedbackDao;

    @Autowired
    public DefaultFeedbackService(final FeedbackDao feedbackDao) {
        this.feedbackDao = Objects.requireNonNull(feedbackDao, "'feedbackDao' parameter must not be null!");
    }

    @NotNull
    @Override
    @Transactional
    public Feedback save(@NotNull final Feedback feedback) {
        Objects.requireNonNull(feedback, "'feedback' parameter must not be null!");

        LOGGER.debug("Feedback '{}' is being saved.", feedback);
        final Feedback savedFeedback = feedbackDao.save(feedback);
        LOGGER.debug("Feedback has been saved as '{}'.", feedback);
        return savedFeedback;
    }

    @NotNull
    @Override
    public Optional<Feedback> findOne(@NotNull final Long id) {
        Objects.requireNonNull(id, "'id' parameter must not be null!");

        LOGGER.debug("Find single feedback record by ID '{}'", id);
        final Feedback record = feedbackDao.findOne(id);
        LOGGER.debug("Found feedback record: '{}'", record);
        return Optional.ofNullable(record);
    }

    @NotNull
    @Override
    public List<Feedback> findAll() {
        LOGGER.debug("Find all feedback records");
        final List<Feedback> records = feedbackDao.findAll();
        LOGGER.debug("Found feedback records: '{}'", records);
        return records;
    }

    @NotNull
    @Override
    public List<Feedback> findAllByUser(final String user) {
        LOGGER.debug("Find all feedback records for user with exact name '{}'", user);
        final List<Feedback> records = feedbackDao.findAllByUser(user);
        LOGGER.debug("Found feedback records: '{}'", records);
        return records;
    }

}
