package com.xmort.domain.entity;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.StringJoiner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.jetbrains.annotations.NotNull;

/**
 * The entity that represents the record with a single user feedback.
 */
@Entity
@Table(name = "feedback")
public class Feedback implements Serializable {

    private static final long serialVersionUID = 6758600564256221137L;

    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private String user;
    @Column(nullable = false)
    private String message;
    @Column(nullable = false, updatable = false)
    private Instant created;

    /**
     * Default constructor of the {@link Feedback} entity.
     * The constructor is required by the Hibernate framework.
     */
    Feedback() {
    }

    /**
     * Create new {@link Feedback} entity.
     * The constructor can be used for easy creation
     *
     * @param user
     *         The name of the user that sent the feedback.
     * @param message
     *         Actual content of the feedback.
     */
    public Feedback(@NotNull final String user, @NotNull final String message) {
        this.user = Objects.requireNonNull(user, "'user' parameter must not be null!");
        this.message = Objects.requireNonNull(message, "'message' parameter must not be null!");
    }

    @PrePersist
    public void setupDateOfCreation() {
        if (created == null) {
            created = Instant.now();
        }
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Feedback feedback = (Feedback) o;
        return Objects.equals(id, feedback.id) &&
                Objects.equals(user, feedback.user) &&
                Objects.equals(message, feedback.message) &&
                Objects.equals(created, feedback.created);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, message, created);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "Feedback{", "}")
                .add("id=" + id)
                .add("user='" + user + "'")
                .add("message='" + message + "'")
                .add("created=" + created)
                .toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(final Instant created) {
        this.created = created;
    }
}
