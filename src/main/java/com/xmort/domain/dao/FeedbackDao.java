package com.xmort.domain.dao;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xmort.domain.entity.Feedback;

/**
 * The repository that manages the {@link Feedback} entity.
 * The repository offers all the basic CRUD operations and various search methods.
 */
@Repository
public interface FeedbackDao extends JpaRepository<Feedback, Long> {

    /**
     * Find all {@link Feedback} records that were sent by provided {@code user}.
     * The method finds the records by exact matching of the provided string to the {@link Feedback#user} field.
     *
     * @param user
     *         The exact user name that the records will be searched for.
     * @return The collection of matching records. In case when no records are matching to the provided {@code user}, the empty collection is returned.
     */
    @NotNull
    List<Feedback> findAllByUser(@Nullable String user);
}
