package com.xmort;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The main class of the Spring Boot application that bootstraps it.
 */
@EnableAutoConfiguration
@SpringBootApplication
public class Application {

    /**
     * The bootstrap method of the Spring Boot application.
     *
     * @param arguments
     *         The start arguments of the application.
     * @throws Exception
     *         The exception is thrown when Spring Boot application did not started for any reason.
     */
    public static void main(final String[] arguments) throws Exception {
        SpringApplication.run(Application.class, arguments);
    }
}
